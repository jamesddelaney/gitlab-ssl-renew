#!/bin/bash

## gitlab-auth-hook.sh
## CertBot manual-auth-hook script used for http-based domain
## validation and authorization.  Script will attempt to publish
## CertBot nonce values to GitLab pages repository.  While loop
## performs the task of waiting for the publishing to complete before
## ending to allow CertBot to continue its validation step.
## 

## GLDirectory is the location on local disk where the GitLab pages repository 
## is currently cloned.  e.g. GLDir="/home/andy/gitlab/my-awesome-blog"
GLDir="[pages clone]"

## SaveDir where in the repo path to save the CertBot validation/token files.
SaveDir="${GLDir}/le"

## URI path that CertBot looks in your domain for the validation/token files.
## Probably won't need a change.
CertPath="/.well-known/acme-challenge/"

## validation/token file writing, commit, and push to git.
echo ${CERTBOT_VALIDATION} > ${SaveDir}/${CERTBOT_TOKEN}
cd ${SaveDir}
git add -- ${CERTBOT_TOKEN}
git commit -m "add certbot token" -- ${CERTBOT_TOKEN}
git push

## waiting for CI/CD publishing to happen
while true; do
    resp=`curl -s -I "http://${CERTBOT_DOMAIN}${CertPath}${CERTBOT_TOKEN}" | grep HTTP | awk '{print $2 }'`
    if [ "${resp}" = "200" ]; then
	break
    fi
    sleep 15
done

